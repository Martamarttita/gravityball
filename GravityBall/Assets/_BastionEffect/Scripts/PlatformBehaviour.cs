﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformBehaviour : MonoBehaviour
{
    [Header("Player Reference")]
    [SerializeField] Transform player;

    [Header("Distance Value")]
    [SerializeField] Vector2 MinAndMaxDistance;

    [Header("Init value Y position")]
    [SerializeField] float yPosition;

    [Header("End value Y position")]
    [SerializeField] float yEndPosition;

    //float distance;
    Vector3 initScale;
    // Start is called before the first frame update
    void Start()
    {
        initScale = transform.localScale;
        transform.Translate(Vector3.up * yPosition);
        transform.localScale = Vector3.zero;
        player = GameObject.Find("Player").GetComponent<Transform>();


    }

    // Update is called once per frame
    void Update()
    {

        // distance = Vector3.Distance(player.position, new Vector3(transform.position.x, player.position.y,transform.position.z));
        var distance = new Vector3(transform.position.x, player.position.y, transform.position.z) - player.position;

        if(distance.sqrMagnitude  <= MinAndMaxDistance.y)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, initScale, Time.deltaTime * 10);
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(transform.position.x, yEndPosition, transform.position.z), Time.deltaTime * 12);
        }
        else
        {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, Time.deltaTime * 10);
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(transform.position.x, yPosition, transform.position.z), Time.deltaTime * 12);
        }
    }
}
